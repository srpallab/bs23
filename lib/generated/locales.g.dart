// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

// ignore_for_file: lines_longer_than_80_chars
// ignore: avoid_classes_with_only_static_members
class AppTranslation {
  static Map<String, Map<String, String>> translations = {
    'bn_BD': Locales.bn_BD,
    'en_US': Locales.en_US,
  };
}

class LocaleKeys {
  LocaleKeys._();
  static const welcome = 'welcome';
  static const button_login = 'button_login';
}

class Locales {
  static const bn_BD = {
    'welcome': 'স্বাগতম',
    'button_login': 'প্রবেশ করুন',
  };
  static const en_US = {
    'welcome': 'Welcome',
    'button_login': 'Login',
  };
}
