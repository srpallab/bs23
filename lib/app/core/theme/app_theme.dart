import 'package:flutter/material.dart';

import 'app_colors.dart';

class AppTheme {
  static ThemeData light = ThemeData.light().copyWith(
      colorScheme: ColorScheme.fromSwatch(
          primarySwatch: AppColors.customThemeColorGraySwatch
      )
  );

  static ThemeData dark = ThemeData.dark().copyWith(
      colorScheme: ColorScheme.fromSwatch(
          primarySwatch: AppColors.customThemeColorGraySwatch
      )
  );
}