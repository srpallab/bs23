import 'package:flutter/material.dart';

import 'app_colors.dart';

class BlankLayout extends StatelessWidget {
  final Widget body;
  const BlankLayout({Key? key, required this.body}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: body);
  }
}

class MainLayout extends StatelessWidget {
  final Widget body;
  final AppBar appbar;
  final bool isDrawer;
  final bool isFloatingButton;
  final Widget? floatingActionButton;
  const MainLayout(
      {Key? key,
      required this.body,
      required this.appbar,
      this.isDrawer = false,
      this.isFloatingButton = false,
      this.floatingActionButton})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.whiteE5,
      appBar: appbar,
      drawer: isDrawer ? const Drawer() : null,
      body: body,
      floatingActionButton: isFloatingButton ? floatingActionButton : null,
    );
  }
}
