import 'package:flutter/material.dart';

class Initializer {
  static Future<void> init() async {
    try {
      WidgetsFlutterBinding.ensureInitialized();
      // await _initStorage();
    } catch (err) {
      rethrow;
    }
  }

  // static Future<void> _initStorage() async {
  //   await GetStorage.init();
  //   Get.put(GetStorage());
  // }
}
