import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'app/routes/app_pages.dart';
import 'app/theme/app_theme.dart';
import 'generated/locales.g.dart';
import 'initializer.dart';

void main() async {
  await Initializer.init();
  runApp(
    GetMaterialApp(
      title: "BS23",
      initialRoute: AppPages.INITIAL,
      getPages: AppPages.routes,
      theme: AppTheme.light,
      darkTheme: AppTheme.dark,
      themeMode: ThemeMode.system,
      translationsKeys: AppTranslation.translations,
      locale: const Locale("en", "US"),
    ),
  );
}
